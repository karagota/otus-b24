# otus-b24



## Домашнее задание №2



В папке doctors лежит страница, которая выводит список докторов. 
<img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%93%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F%20-%20%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D0%B4%D0%BE%D0%BA%D1%82%D0%BE%D1%80%D0%BE%D0%B2.jpg" />
При клике на карточку доктора загружается страница с его данными, а также списком процедур и специализаций, которые с ним связаны.
<img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%A1%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0%20%D0%B2%D1%80%D0%B0%D1%87%D0%B0.jpg" />

На странице со списком врачей доступны две кнопки, ведущие на страницы 
 - добавление врача
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%94%D0%BE%D0%B1%D0%B0%D0%B2%D0%B8%D1%82%D1%8C%20%D0%B2%D1%80%D0%B0%D1%87%D0%B0.jpg" />
 - добавление процедуры
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%B4%D0%BE%D0%B1%D0%B0%D0%B2%D0%B8%D1%82%D1%8C%20%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D0%B4%D1%83%D1%80%D1%83.jpg" />

На странице конкретного врача доступна одна кнопка
 - изменение данных врача (в т.ч. можно изменить список процедур, связанных с врачом)
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B8%D1%82%D1%8C%20%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5%20%D0%B2%D1%80%D0%B0%D1%87%D0%B0.jpg" />

 В админке списки выглядят так:
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D0%B2%D1%80%D0%B0%D1%87%D0%B5%D0%B9%20%D0%B2%20%D0%B0%D0%B4%D0%BC%D0%B8%D0%BD%D0%BA%D0%B5.jpg" />
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%9F%D0%BE%D0%BB%D1%8F%20%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%B0%20%D0%B2%D1%80%D0%B0%D1%87%D0%B5%D0%B9.jpg" />
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D0%BF%D1%80%D0%BE%D1%86%D0%B5%D0%B4%D1%83%D1%80%20%D0%B2%20%D0%B0%D0%B4%D0%BC%D0%B8%D0%BD%D0%BA%D0%B5.png" />
 <img src="https://gitlab.com/karagota/otus-b24/-/raw/main/upload/screenshots/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA%20%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B9.jpg" />


В проекте используются классы из папки local/app/models

xml-экспорт инфоблоков - в папке /upload

скриншоты с инсталляции на BitrixVM - в папке /upload/screenshots
 
