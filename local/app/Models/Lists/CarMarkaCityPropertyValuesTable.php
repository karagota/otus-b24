<?php
namespace Models\Lists;
use Models\AbstractIblockPropertyValuesTable;
class CarMarkaCityPropertyValuesTable extends AbstractIblockPropertyValuesTable
{
    public const IBLOCK_ID = 18;

    public static function getMap(): array {
        return parent::getMap();
    }
}