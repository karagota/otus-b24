<?php
namespace Models\Lists;
use Bitrix\Main\Entity\Query\Join;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ORM\Fields\Relations\ManyToMany;
use Models\AbstractIblockPropertyValuesTable;

class DoctorsPropertyValuesTable extends AbstractIblockPropertyValuesTable
{
    public const IBLOCK_ID = 19;

    public static function getMap(): array {
        $map = [
            "SPECS" => (new ReferenceField(
                'SPECS',
                SpecsPropertyValuesTable::class,
                Join::on('this.SPEC_IDS','in', 'ref.IBLOCK_ELEMENT_ID'),
                ['join_type' => 'INNER']))

            ];

       return parent::getMap()+$map;
    }

}
