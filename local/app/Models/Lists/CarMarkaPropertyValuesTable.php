<?php
namespace Models\Lists;
use Models\AbstractIblockPropertyValuesTable;
class CarMarkaPropertyValuesTable extends AbstractIblockPropertyValuesTable
{
    public const IBLOCK_ID = 17;

    public static function getMap(): array {
        return parent::getMap();
    }
}